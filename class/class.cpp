#include <iostream>
using namespace std;

class Player{
    public:
        int x, y;
        int speed;
    
    //method:
    void move(int x_step, int y_step){
        x += x_step * speed;
        y += y_step * speed;
    }

    void printPosition(){
        cout << "(x,y) = " << "(" << x << "," << y << ")" << endl;
    }
};

int main(){
    Player player;
    player.x = 5;
    player.y = 8;
    player.speed = 2;
    player.printPosition();
    player.move(3, 5);
    player.printPosition();
}