#include <iostream>
#include <string.h>

using namespace std;

#define Log(x) cout << x << endl;

int main(){
    int x = 5;
    int *xptr = &x;
    Log(x);
    Log(xptr);
    Log(*xptr);
    
    //using dereference to change the value of the value
    *xptr = 6;
    Log(x);

    char mychar[8];
    Log(&mychar);
    char *buffer = mychar;
    memset(buffer, 'c', 8);
    printf("%p\n", buffer);
}
