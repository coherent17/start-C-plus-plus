#include <iostream>
using namespace std;

void increment(int *value){
    (*value)++;
}

void add(int &value){
    value++;
}

int main(){
    //reference work as the same as the origin variable
    int a = 5;
    int &ref = a;
    ref = 2;
    cout << a << endl;
    
    //change value through pointer
    int b = 3;
    increment(&b);
    cout << b << endl;

    //change value by reference
    int c = 9;
    add(c);
    cout << c << endl;
}