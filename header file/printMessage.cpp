#include <iostream>
#include "printMessage.h"
using namespace std;

void printMessage(const char *message){
    cout << message << endl;
}

void initMessage(){
    printMessage("Initialize message");
}