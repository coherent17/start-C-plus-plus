#include <iostream>
#include "../memory_monitor.h"

int main(){
    int x = 5;
    int *xptr = &x;
    printf("xptr = %p\n", xptr);
    memory_dump(&xptr, sizeof(int *));
}