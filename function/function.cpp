#include <iostream>
using namespace std;

int multiply(int a, int b){
    return a * b;
}

void printMultiply(int a, int b){
    cout << multiply(a, b) << endl;
}

int main(){
    printMultiply(3, 4);
    printMultiply(39, 24);
    printMultiply(398, 443);
    printMultiply(352, 44);
}