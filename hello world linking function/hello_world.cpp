#include <iostream>
using namespace std;

void printString(const char *);

int main(){
    cout << "Hello World" << endl;
    printString("Nice Job");
}

//compile command:
// $ g++ -g -Wall function_call_in_hello_world.cpp hello_world.cpp
