#include <iostream>
using namespace std;

int main(){
    int a = 8;
    cout << a << endl;
    cout << sizeof(a) << endl; //4

    float b = 5.5f;
    cout << b << endl;
    cout << sizeof(b) << endl; //4

    double c = 6.3;
    cout << c << endl;
    cout << sizeof(c) << endl; //8

    bool d = true;
    cout << d << endl;
    cout << sizeof(d) << endl; //1
}