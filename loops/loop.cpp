#include <iostream>
using namespace std;

void printMessage(const char *message){
    cout << message << endl;
}

int main(){

    //for loops
    for (int i = 0; i < 5;i++){
        printMessage("Hello World");
    }
    printMessage("------------------");

    //while loops
    int i = 0;
    while(i<5){
        printMessage("Hello World");
        i++;
    }
    printMessage("------------------");

    //do while loops
    i = 0;
    do{
        printMessage("Hello World");
    } while (++i < 5);
}